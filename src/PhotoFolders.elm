module PhotoFolders exposing (Model, Msg, init, main, merge, pageTitle, update, view)

import Browser
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (class, href, src)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (Decoder, int, list, string, succeed)
import Json.Decode.Pipeline exposing (required)
import UrlPrefix


type Folder
    = Folder
        { name : String
        , photoUrls : List String
        , subfolders : List Folder
        , expanded : Bool
        }


type alias Model =
    { selectedPhotoUrl : Maybe String
    , photos : Dict String Photo
    , root : Folder
    }


initialModel : Model
initialModel =
    { selectedPhotoUrl = Nothing
    , photos = Dict.empty
    , root =
        Folder
            { name = "Loading..."
            , photoUrls = []
            , subfolders = []
            , expanded = True
            }
    }


init : Maybe String -> ( Model, Cmd Msg )
init selectedFilename =
    ( { initialModel | selectedPhotoUrl = selectedFilename }
    , Http.get
        { url = "http://elm-in-action.com/folders/list"
        , expect = Http.expectJson GotInitialModel modelDecoder
        }
    )


merge : { oldModel : Model, newModel : Model } -> Model
merge { oldModel, newModel } =
    let
        root : Folder
        root =
            newModel.selectedPhotoUrl
                |> Maybe.andThen (findUrl oldModel.root)
                |> Maybe.map (expandPath oldModel.root)
                |> Maybe.withDefault oldModel.root
    in
    { oldModel
        | selectedPhotoUrl = newModel.selectedPhotoUrl
        , root = root
    }


modelDecoder : Decoder Model
modelDecoder =
    Decode.map
        (\( root, photos ) ->
            { photos = photos
            , root = root
            , selectedPhotoUrl = Nothing
            }
        )
        folderDecoder


type Msg
    = GotInitialModel (Result Http.Error Model)
    | ClickedFolder FolderPath


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickedFolder folderPath ->
            ( { model | root = toggleExpanded folderPath model.root }, Cmd.none )

        GotInitialModel (Ok newModel) ->
            ( { newModel | selectedPhotoUrl = model.selectedPhotoUrl }, Cmd.none )

        GotInitialModel (Err _) ->
            ( model, Cmd.none )


pageTitle : String
pageTitle =
    "Folders"


view : Model -> Html Msg
view model =
    let
        photoByUrl : String -> Maybe Photo
        photoByUrl url =
            Dict.get url model.photos

        selectedPhoto : Html Msg
        selectedPhoto =
            case Maybe.andThen photoByUrl model.selectedPhotoUrl of
                Just photo ->
                    viewSelectedPhoto photo

                Nothing ->
                    text ""
    in
    div [ class "content" ]
        [ div [ class "folders" ]
            [ viewFolder End model.root
            ]
        , div [ class "selected-photo" ] [ selectedPhoto ]
        ]


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init Nothing
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }


type alias Photo =
    { title : String
    , size : Int
    , relatedUrls : List String
    , url : String
    }


viewPhoto : String -> Html Msg
viewPhoto url =
    a [ href ("/photos/" ++ url), class "photo" ]
        [ text url ]


viewSelectedPhoto : Photo -> Html Msg
viewSelectedPhoto photo =
    div
        [ class "selected-photo"
        ]
        [ h2 [] [ text photo.title ]
        , img [ src (UrlPrefix.fullUrl photo.url) ] []
        , span [] [ text <| String.fromInt photo.size ++ "KB" ]
        , h3 [] [ text "Related" ]
        , div [ class "related-photos" ] <| List.map viewRelatedPhotos photo.relatedUrls
        ]


viewRelatedPhotos : String -> Html Msg
viewRelatedPhotos url =
    a [ href ("/photos/" ++ url) ]
        [ img
            [ class "related-photo"
            , src (UrlPrefix.thumbnailUrl url)
            ]
            []
        ]


viewFolder : FolderPath -> Folder -> Html Msg
viewFolder path (Folder folder) =
    let
        viewSubfolder : Int -> Folder -> Html Msg
        viewSubfolder index =
            viewFolder (appendIndex index path)

        folderLabel : Html Msg
        folderLabel =
            label [ onClick (ClickedFolder path) ] [ text folder.name ]
    in
    if folder.expanded then
        let
            contents =
                List.append
                    (List.indexedMap viewSubfolder folder.subfolders)
                    (List.map viewPhoto folder.photoUrls)
        in
        div [ class "folder expanded" ]
            [ folderLabel
            , div [ class "contents" ] contents
            ]

    else
        div [ class "folder collapsed" ]
            [ folderLabel
            ]


appendIndex : Int -> FolderPath -> FolderPath
appendIndex index path =
    case path of
        End ->
            Subfolder index End

        Subfolder subfolderIndex remainingPath ->
            Subfolder subfolderIndex <| appendIndex index remainingPath


type FolderPath
    = End
    | Subfolder Int FolderPath


traversePath : (Bool -> Folder -> Folder) -> FolderPath -> Folder -> Folder
traversePath mapFolder path (Folder folder) =
    case path of
        End ->
            mapFolder True (Folder folder)

        Subfolder index nestedPath ->
            let
                subfolders : List Folder
                subfolders =
                    List.indexedMap transform folder.subfolders

                transform : Int -> Folder -> Folder
                transform currentIndex subfolder =
                    if index == currentIndex then
                        traversePath mapFolder nestedPath subfolder

                    else
                        subfolder
            in
            mapFolder False (Folder { folder | subfolders = subfolders })


toggleExpanded : FolderPath -> Folder -> Folder
toggleExpanded path folder =
    let
        toggleFolder : Bool -> Folder -> Folder
        toggleFolder isLeaf (Folder subfolder) =
            if isLeaf then
                Folder { subfolder | expanded = not subfolder.expanded }

            else
                Folder subfolder
    in
    traversePath toggleFolder path folder


expandPath : Folder -> FolderPath -> Folder
expandPath folder path =
    let
        expandFolder : Bool -> Folder -> Folder
        expandFolder _ (Folder subfolder) =
            Folder { subfolder | expanded = True }
    in
    traversePath expandFolder path folder


findUrl : Folder -> String -> Maybe FolderPath
findUrl (Folder folder) url =
    let
        foundPhoto : Bool
        foundPhoto =
            List.any ((==) url) folder.photoUrls
    in
    if foundPhoto then
        Just End

    else
        let
            mapPath : ( Int, Folder ) -> Maybe FolderPath
            mapPath ( index, subfolder ) =
                findUrl subfolder url
                    |> Maybe.map (Subfolder index)

            reducer : ( Int, Folder ) -> Maybe FolderPath -> Maybe FolderPath
            reducer folderTuple maybePath =
                case maybePath of
                    Just path ->
                        Just path

                    Nothing ->
                        mapPath folderTuple
        in
        List.indexedMap Tuple.pair folder.subfolders
            |> List.foldl reducer Nothing


type alias JsonPhoto =
    { title : String
    , size : Int
    , relatedUrls : List String
    }


jsonPhotoDecoder : Decoder JsonPhoto
jsonPhotoDecoder =
    succeed JsonPhoto
        |> required "title" string
        |> required "size" int
        |> required "related_photos" (list string)


finishPhoto : ( String, JsonPhoto ) -> ( String, Photo )
finishPhoto ( url, { title, size, relatedUrls } ) =
    ( url
    , { title = title
      , size = size
      , relatedUrls = relatedUrls
      , url = url
      }
    )


fromPairs : List ( String, JsonPhoto ) -> Dict String Photo
fromPairs pairs =
    pairs
        |> List.map finishPhoto
        |> Dict.fromList


photosDecoder : Decoder (Dict String Photo)
photosDecoder =
    Decode.keyValuePairs jsonPhotoDecoder
        |> Decode.map fromPairs


folderDecoder : Decoder ( Folder, Dict String Photo )
folderDecoder =
    Decode.succeed folderFromJson
        |> required "name" string
        |> required "photos" photosDecoder
        |> required "subfolders" (Decode.lazy (\_ -> list folderDecoder))


folderFromJson : String -> Dict String Photo -> List ( Folder, Dict String Photo ) -> ( Folder, Dict String Photo )
folderFromJson name photos subdata =
    let
        ( subfolders, subphotos ) =
            List.unzip subdata
    in
    ( Folder
        { name = name
        , expanded = True
        , subfolders = subfolders
        , photoUrls = Dict.keys photos
        }
    , List.foldl Dict.union photos subphotos
    )

module UrlPrefix exposing (..)


urlPrefix : String
urlPrefix =
    "http://elm-in-action.com/"


thumbnailUrl : String -> String
thumbnailUrl url =
    urlPrefix ++ "photos/" ++ url ++ "/thumb"


fullUrl : String -> String
fullUrl url =
    urlPrefix ++ "photos/" ++ url ++ "/full"


largeUrl : String -> String
largeUrl url =
    urlPrefix ++ "large/" ++ url


thumbUrl : String -> String
thumbUrl url =
    urlPrefix ++ url
